(function(){
    'use strict';

   angular.module("myApp.controller", [])
        .controller("myCtrl", function($scope,myService, $window) {

            //get all objects from myService
            myService.getAllObjects().then(function(response){
                $scope.objects = response;

                //fitler objects with "air conditioners" category
                $scope.filteredObjects = _.filter($scope.objects, function(o){
                    return o.category === "Air Conditioners";
                });
                console.log($scope.filteredObjects);
            });

            $scope.total = 0;
            $scope.averageCubicWeight = 0;
            //trigger getTotal function
            $scope.getTotal = function(){
                //for loop to get total cubic weight of all objs from "air conditioners"
                for(var i = 0; i < $scope.filteredObjects.length; i++){
                    var product = $scope.filteredObjects[i];
                    $scope.total += (product.size.width/100 * product.size.length/100 * product.size.height/100);
                }

                //define average cubic weight into $scope
                $scope.averageCubicWeight = $scope.total/$scope.filteredObjects.length *250;
                return $scope.averageCubicWeight;
            };
    });
})();