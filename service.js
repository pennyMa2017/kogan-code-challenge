(function(){
    'use strict';

    //define a factory in module 'myApp.service'
    angular.module('myApp.service', [])
        .factory('myService', myService);

        function myService($http){
            //create variable objects
            var objects = {};
            //create function getAllObjects
            objects.getAllObjects = function(){
                var req = {
                    method: 'GET',
                    url:'http://wp8m3he1wt.s3-website-ap-southeast-2.amazonaws.com/api/products/1'
                };
                //return $http promise
                return $http(req).then(function(response){
                    console.log(response);
                    return response.data.objects;
                }, function(error){
                    console.log(error);
                });
            };
            return objects;
        }
})();
        