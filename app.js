(function (){
    'use strict';
    
    angular.module('myApp', [
        'myApp.service',
        'myApp.controller'
    ]);

})()
